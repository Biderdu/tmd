import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-head-panel',
  templateUrl: './head-panel.component.html',
  styleUrls: ['./head-panel.component.scss']
})
export class HeadPanelComponent implements OnInit {
  @Input() user: any;

  constructor() { }

  ngOnInit() {

  }

}
