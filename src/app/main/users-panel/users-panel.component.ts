import { Component, OnInit } from '@angular/core';
import { AuthService, UserItem } from 'src/app/services/auth.service';

@Component({
  selector: 'app-users-panel',
  templateUrl: './users-panel.component.html',
  styleUrls: ['./users-panel.component.scss']
})

export class UsersPanelComponent implements OnInit {
  private userList: Array<UserItem>;

  constructor(private authService: AuthService) {
    this.userList = [];
    // this.userList = [
    //   {
    //     name: 'Admin',
    //     mail: 'admin_tmd@gmail.com',
    //     status: 'active',
    //   },
    //   {
    //     name: 'Manager',
    //     mail: 'manager_tmd@gmail.com',
    //     status: 'active',
    //   },
    //   {
    //     name: 'User',
    //     mail: 'user_tmd@gmail.com',
    //     status: 'active',
    //   }
    // ];
  }

  ngOnInit() {
    this.userList = this.authService.getUsers();
    console.log('INIT USERS', this.userList);
  }

  block(user) {
    console.log(user);
  }

  togglePanel(mode) {
    console.log(mode);
  }
}
