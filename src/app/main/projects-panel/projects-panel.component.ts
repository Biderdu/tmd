import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FirebaseService } from 'src/app/services/fireb.service';
import { AuthService } from 'src/app/services/auth.service';
import { ProjectsService } from 'src/app/services/projects.service';

export interface NewProject {
  name: string;
  owner: string;
  idCount: number;
}

@Component({
  selector: 'app-projects-panel',
  templateUrl: './projects-panel.component.html',
  styleUrls: ['./projects-panel.component.scss']
})
export class ProjectsPanelComponent implements OnInit {
  private projectsList: Array<any>;
  private newProjectName: string;
  private showNewProjectLayout: boolean;

  @Output() setProjectEvent = new EventEmitter<{project: string}>();

  constructor(private firebaseService: FirebaseService, private authService: AuthService, private projectsService: ProjectsService) {
    this.showNewProjectLayout = false;

    this.projectsList = [];
  }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    this.projectsService.getAvailableProjects(this.authService.getUserInfo().user).subscribe(
      () => {},
      (error) => { console.log(error); },
      () => {
        this.projectsList = this.projectsService.getProjectsList();
      }
    );
  }

  selectProject(project) {
    this.setProjectEvent.emit({project: project.id});
  }

  createProject() {

    const idTaskName = this.newProjectName ? this.newProjectName[0] + this.newProjectName[1] : 'ID';

    const newProject = {
      name: this.newProjectName,
      owner: this.authService.getUserInfo().user,
      idCount: 0,
      idName: idTaskName,
    };

    this.firebaseService.createProject(newProject).subscribe((data) => {
      this.projectsList.unshift(
        {
          ...newProject,
          id: data.id
        });
      this.showNewProjectLayout = false;
    },
    (error) => {
      console.log('NEW PROJECT ERROR', error);
    });
  }

}
