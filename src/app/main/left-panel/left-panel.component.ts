import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.scss']
})
export class LeftPanelComponent implements OnInit {
  @Output() panelEvent = new EventEmitter<string>();
  @Input() accessAdmin: boolean;

  constructor() { }

  ngOnInit() {
  }

  activatePanel(value: string) {
    this.panelEvent.emit(value);
  }

}
