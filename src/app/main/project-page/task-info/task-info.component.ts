import { Component, OnInit, Input } from '@angular/core';
import { FirebaseService } from 'src/app/services/fireb.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-task-info',
  templateUrl: './task-info.component.html',
  styleUrls: ['./task-info.component.scss']
})
export class TaskInfoComponent implements OnInit {
  @Input() private selectedTask: any;

  private userName: string;

  private user: any;

  private commentsList: Array<any>;

  private newComment: string;

  constructor(private firebaseService: FirebaseService, private authService: AuthService) { }

  ngOnInit() {
    this.user = this.authService.getUserInfo();

    this.getUserInfo();
    this.getComments();
  }

  getComments() {

    this.firebaseService.getComments(this.selectedTask).subscribe((commentsData) => {
      this.commentsList = commentsData.docs.map((data) => {
        return({
            ...data.data(),
            id: data.id
        });
      });
    });

  }

  getUserInfo() {
    if (!this.selectedTask.user) { return; }

    this.firebaseService.getAllInfo(this.selectedTask.user).subscribe((roleData) => {
      if (!roleData.empty) {
        const userInfo = roleData.docs[0].data();
        this.userName = userInfo.name;
      }

    }, (error) => {

    });
  }

  addComment() {
    const newComment = {
      text: this.newComment,
      user: this.user.name,
      project: this.selectedTask.project,
      task: this.selectedTask.id,
    };

    this.firebaseService.createComment(newComment).subscribe((data) => {
      this.commentsList.push(newComment);
      this.newComment = '';
    });

  }

}
