import { Component, OnInit, Input } from '@angular/core';
import { ProjectsService } from 'src/app/services/projects.service';
import { FirebaseService } from 'src/app/services/fireb.service';
import { FormControl } from '@angular/forms';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

export interface NewSprint {
  project: string;
  name: string;
  startDate: string;
  endDate: string;
}

@Component({
  selector: 'app-sprints',
  templateUrl: './sprints.component.html',
  styleUrls: ['./sprints.component.scss']
})
export class SprintsComponent implements OnInit {
  @Input() private selectedProject: 'string';

  private picker: string;
  private matDatepicker: any;

  private activeProject: any;
  private sprintsList: Array<any>;
  private taskList: Array<any>;
  private allTasksList: Array<any>;

  private newSprintName: string;
  private startSprintDate: Date;
  private endSprintDate: Date;

  private showRetroLayout: boolean;
  private showRetroInfo: boolean;
  private activeRetroSprint: any;
  private retroTodo: string;
  private retroStop: string;
  private retroIdeas: string;

  constructor(private projectsService: ProjectsService, private firebaseService: FirebaseService) {}

  ngOnInit() {
    this.activeProject = this.projectsService.getProjectInfo(this.selectedProject);

    this.showRetroLayout = false;
    this.showRetroInfo = false;
    this.activeRetroSprint = null;

    this.sprintsList = [];
    this.allTasksList = [];
    this.taskList = [];

    this.getSprints();
  }

  getSprints() {
    this.projectsService.getSprints(this.activeProject.id).subscribe((sprintList) => {
      this.sprintsList = sprintList;
      this.getTasks();
    });
  }

  getTasks() {
    this.projectsService.getTasks(this.activeProject.id).subscribe((taskList) => {
      this.allTasksList = taskList;
      this.sortTasks();
    });
  }

  sortTasks() {
    this.sprintsList.forEach((sprint) => {
      sprint.tasks = this.allTasksList.filter(task => task.sprint === sprint.id);
    });

    this.taskList = this.allTasksList.filter(task => !task.sprint);
  }

  createSprint() {
    const startDate = `${(this.startSprintDate.getMonth() + 1)}, ${this.startSprintDate.getDate()} ${this.startSprintDate.getFullYear()}`;
    const endDate = `${(this.endSprintDate.getMonth() + 1)}, ${this.endSprintDate.getDate()} ${this.endSprintDate.getFullYear()}`;

    const newSprint = {
      project: this.activeProject.id,
      name: this.newSprintName,
      closed: false,
      startDate,
      endDate,
    };

    this.firebaseService.createSprint(newSprint).subscribe((sprintDocumet) => {
      this.sprintsList.unshift({
        ...newSprint,
        id: sprintDocumet.id,
        tasks: []
      });

      this.resetNewSprint();
    },
    (error) => {

    });
  }

  retroSprint(sprint) {
    this.activeRetroSprint = sprint;

    if (sprint.retro) {
      this.showRetroInfo = true;
    } else {
      this.showRetroLayout = true;
    }

    this.retroTodo = sprint.retroTodo;
    this.retroStop = sprint.retroStop;
    this.retroIdeas = sprint.retroIdeas;
  }

  editRetro() {
    this.showRetroInfo = false;
    this.showRetroLayout = true;
  }

  addRetro() {
    const updated = {
      retro: true,
      retroTodo: this.retroTodo,
      retroStop: this.retroStop,
      retroIdeas: this.retroIdeas,
    };

    this.firebaseService.updateSprint(this.activeRetroSprint, updated).subscribe((data) => {
      this.activeRetroSprint.retro = true;
      this.activeRetroSprint.retroTodo = this.retroTodo;
      this.activeRetroSprint.retroStop = this.retroStop;
      this.activeRetroSprint.retroIdeas = this.retroIdeas;

      this.showRetroLayout = false;
      this.showRetroInfo = true;
    });
  }

  deleteRetro() {
    const updated = {
      retro: false,
      retroTodo: '',
      retroStop: '',
      retroIdeas: '',
    };

    this.firebaseService.updateSprint(this.activeRetroSprint, updated).subscribe((data) => {
      this.activeRetroSprint.retro = false;
      this.activeRetroSprint.retroTodo = '';
      this.activeRetroSprint.retroStop = '';
      this.activeRetroSprint.retroIdeas = '';

      this.showRetroInfo = false;
      this.activeRetroSprint = null;
    });
  }

  closeSprint(sprint) {
    this.firebaseService.updateSprint(sprint, {closed: true}).subscribe((data) => {
      sprint.closed = true;
      console.log('updated', data);
    });
  }

  deleteSprint(sprint) {
    this.firebaseService.deleteSprint(sprint).subscribe(
      () => {
        this.sprintsList = this.sprintsList.filter(t => t !== sprint);
      },
      () => {
        console.log('DELETE ERROR!');
      }
    );
  }

  resetNewSprint() {
    this.newSprintName = '';

    this.startSprintDate = null;
    this.endSprintDate = null;
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      console.log('MOVE ITEM');
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);

      const task = event.container.data[event.currentIndex];

      this.checkTaskInfo(task);
    }
  }

  checkTaskInfo(task) {
    this.taskList.forEach((ts) => {
      if (ts.id === task.id) {
        task.sprint = null;
      }
    });

    this.sprintsList.forEach((sprint) => {
      sprint.tasks.forEach((ts) => {
        if (ts.id === task.id) {
          task.sprint = sprint.id;
        }
      });
    });

    this.updateTask(task);
  }

  updateTask(task) {
    this.firebaseService.updateTask(task, {sprint: task.sprint}).subscribe((data) => {
      console.log('updated', data);
    });
  }

  //
  getId(sprint) {
    return 'task-list-' + sprint.id;
  }

  getAllIds() {
    return this.sprintsList.map(sprint => this.getId(sprint));
  }

}
