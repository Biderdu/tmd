import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FirebaseService } from 'src/app/services/fireb.service';
import { ProjectsService } from 'src/app/services/projects.service';

export interface NewTask {
  uid: string;
  title: string;
  status: string;
  description: string;
  project: string;
}

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.scss']
})
export class ProjectPageComponent implements OnInit {
  @Input() private selectedProject: 'string';

  private panelState: string;

  private activeProject: any;

  @Output() leaveProjectEvent = new EventEmitter();

  constructor(private firebaseService: FirebaseService, private projectsService: ProjectsService) {
    // this.showNewTaskLayout = false;
    // this.taskList = [];
    // this.teamList = [];

    this.panelState = 'backlog';

    // this.showTaskInfo = false;
    // this.selectedTask = null;
  }

  ngOnInit() {
    this.activeProject = this.projectsService.getProjectInfo(this.selectedProject);
    // this.idName = this.activeProject.idName || 'ID';
    // this.idCount = this.activeProject.idCount || 0;

    console.log('ACTIVE PROJECT', this.activeProject);

    // this.getTasks();
    // this.getTeam();
  }


  deleteProject() {
    this.projectsService.deleteProject(this.activeProject.id).subscribe(() => {}, () => {}, () => {
      this.leaveProjectEvent.emit();
    });
  }

  showTasksPanel(panel: string) {
    return panel === this.panelState;
  }

  togglePanel(panel: string) {
    this.panelState = panel;
  }
}
