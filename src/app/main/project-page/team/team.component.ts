import { Component, OnInit, Input } from '@angular/core';
import { ProjectsService } from 'src/app/services/projects.service';
import { FirebaseService } from 'src/app/services/fireb.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
  @Input() private selectedProject: 'string';

  private activeProject: any;

  private userList: Array<any>;
  private newUser: any;

  private teamList: Array<any>;

  constructor(private projectsService: ProjectsService, private authService: AuthService, private firebaseService: FirebaseService) { }

  ngOnInit() {
    this.userList = this.authService.getUsers();

    this.activeProject = this.projectsService.getProjectInfo(this.selectedProject);

    this.getTeam();
  }

  getTeam() {
    this.projectsService.getTeam(this.activeProject.id).subscribe((teamList) => {
      this.teamList = teamList;
    });
  }

  addUser() {
    const newUser = {
      name: this.newUser.name,
      project: this.activeProject.id,
      user: this.newUser.user,
      role: this.newUser.role,
    };

    this.firebaseService.addTeamUser(newUser).subscribe((teamDocumet) => {
      this.teamList.unshift({
        ...newUser,
        id: teamDocumet.id
      });
    },
    (error) => {

    });
  }

  deleteTeamUser(user) {
    this.firebaseService.deleteTeamUser(user).subscribe(
      () => {
        this.teamList = this.teamList.filter(t => t !== user);
      },
      () => {
        console.log('DELETE ERROR!');
      }
    );
  }
}
