import { Component, OnInit, Input } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ProjectsService } from 'src/app/services/projects.service';
import { FirebaseService } from 'src/app/services/fireb.service';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit {
  @Input() private selectedProject: 'string';

  private todo: Array<any>;
  private progress: Array<any>;
  private done: Array<any>;
  private clarify: Array<any>;

  private activeSprint: any;

  private activeProject: any;
  private sprintsList: Array<any>;
  // private taskList: Array<any>;
  private allTasksList: Array<any>;

  constructor(private projectsService: ProjectsService, private firebaseService: FirebaseService) {
    this.todo = [];
    this.done = [];
    this.clarify = [];
    this.progress = [];
   }

  ngOnInit() {
    this.activeProject = this.projectsService.getProjectInfo(this.selectedProject);

    this.sprintsList = [];
    this.allTasksList = [];
    // this.taskList = [];

    this.getSprints();
  }

  getSprints() {
    this.projectsService.getSprints(this.activeProject.id).subscribe((sprintList) => {
      this.sprintsList = sprintList;
      this.getTasks();
    });
  }

  getTasks() {
    this.projectsService.getTasks(this.activeProject.id).subscribe((taskList) => {
      this.allTasksList = taskList;
      this.sortTasks();
    });
  }

  sortTasks() {
    this.sprintsList.forEach((sprint) => {
      sprint.tasks = this.allTasksList.filter(task => task.sprint === sprint.id);
    });

    // this.taskList = this.allTasksList.filter(task => !task.sprint);
  }

  selectSprint(sprintId) {
    console.log(sprintId);

    this.todo = [];
    this.progress = [];
    this.done = [];
    this.clarify = [];

    this.sprintsList.forEach((spr) => {
      if (spr.id === sprintId) {
        this.activeSprint = spr;
      }
    });

    if (this.activeSprint) {
      this.activeSprint.tasks.forEach((task) => {
        if (task.status === 'progress') {
          this.progress.push(task);
        } else if (task.status === 'done') {
          this.done.push(task);
        } else if (task.status === 'clarify') {
          this.clarify.push(task);
        } else {
          this.todo.push(task);
        }
      });
    }

  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);

      const task = event.container.data[event.currentIndex];

      this.checkTaskInfo(task);
    }
  }

  checkTaskInfo(task) {
    this.todo.forEach((ts) => {
      if (ts.id === task.id) {
        task.status = 'todo';
      }
    });

    this.progress.forEach((ts) => {
      if (ts.id === task.id) {
        task.status = 'progress';
      }
    });

    this.done.forEach((ts) => {
      if (ts.id === task.id) {
        task.status = 'done';
      }
    });

    this.clarify.forEach((ts) => {
      if (ts.id === task.id) {
        task.status = 'clarify';
      }
    });

    this.updateTask(task);
  }

  updateTask(task) {
    console.log('updateTask', task);
    this.firebaseService.updateTask(task, {status: task.status}).subscribe((data) => {
      console.log('updated', data);
    });
  }

}
