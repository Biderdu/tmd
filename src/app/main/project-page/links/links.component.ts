import { Component, OnInit, Input } from '@angular/core';
import { ProjectsService } from 'src/app/services/projects.service';
import { FirebaseService } from 'src/app/services/fireb.service';

export interface NewLink {
  project: string;
  name: string;
  link: string;
}

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})

export class LinksComponent implements OnInit {
  @Input() private selectedProject: 'string';

  private activeProject: any;

  private showNewLinkLayout: boolean;
  private newLinkName: string;
  private newLinkUrl: string;

  private linksList: Array<any>;

  constructor(private projectsService: ProjectsService, private firebaseService: FirebaseService) {
    this.showNewLinkLayout = false;
    this.linksList = [];
  }

  ngOnInit() {
    this.activeProject = this.projectsService.getProjectInfo(this.selectedProject);

    this.getLinks();
  }

  getLinks() {
    this.firebaseService.getLinks(this.activeProject.id).subscribe((linksData) => {
      linksData.docs.forEach((data) => {
        this.linksList.push({
            ...data.data(),
            id: data.id
        });
      });
    }, (error) => {
      console.log('GET SPRINTS ERROR', error);
    });
  }

  createLink() {
    const newLink = {
      project: this.activeProject.id,
      name: this.newLinkName,
      link: this.newLinkUrl,
    };

    this.firebaseService.createLink(newLink).subscribe((linkDocumet) => {
      this.linksList.unshift({
        ...newLink,
        id: linkDocumet.id
      });

      this.resetNewLink();
    },
    (error) => {

    });
  }

  resetNewLink() {
    this.showNewLinkLayout = false;
    this.newLinkName = '';
    this.newLinkUrl = '';
  }

  
  deleteLink(link) {
    this.firebaseService.deleteLink(link).subscribe(
      () => {
        console.log('DELETE SUCCESSFULL');
        this.linksList = this.linksList.filter(t => t !== link);
      },
      () => {
        console.log('DELETE ERROR!');
      }
    );
  }
}
