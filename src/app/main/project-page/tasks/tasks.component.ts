import { Component, OnInit, Input } from '@angular/core';
import { ProjectsService } from 'src/app/services/projects.service';
import { FirebaseService } from 'src/app/services/fireb.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  @Input() private selectedProject: 'string';

  private taskList: Array<any>;
  private teamList: Array<any>;
  private showNewTaskLayout: boolean;
  private showEditTaskLayout: boolean;

  private newTaskName: string;
  private newTaskDescription: string;
  private newTaskUser: any;
  private endTaskDate: Date;

  private editTaskName: string;
  private editTaskDescription: string;
  private editTaskUser: any;
  private editTaskEndDate: Date;

  private taskToEdit: any;

  private picker: string;
  private matDatepicker: any;

  private selectedTask: any;
  private showTaskInfo: boolean;

  private idName: string;
  private idCount: number;

  private activeProject: any;

  constructor(private projectsService: ProjectsService, private firebaseService: FirebaseService) {
    this.showNewTaskLayout = false;
    this.showEditTaskLayout = false;
    this.taskList = [];
    this.teamList = [];

    this.taskToEdit = null;
    this.showTaskInfo = false;
    this.selectedTask = null;
   }

  ngOnInit() {
    this.activeProject = this.projectsService.getProjectInfo(this.selectedProject);
    this.idName = this.activeProject.idName || 'ID';
    this.idCount = this.activeProject.idCount || 0;

    this.getTasks();
    this.getTeam();
  }

   //
   getTeam() {
    this.projectsService.getTeam(this.activeProject.id).subscribe((teamList) => {
      this.teamList = teamList;
    });
  }

  //
  getTasks() {
    this.projectsService.getTasks(this.activeProject.id).subscribe((taskList) => {
      this.taskList = taskList;
    });
  }

  selectTask(task) {
    this.showTaskInfo = true;
    this.selectedTask = task;
  }

  createTask() {
    const endDate = `${(this.endTaskDate.getMonth() + 1)}, ${this.endTaskDate.getDate()} ${this.endTaskDate.getFullYear()}`;

    const newTask = {
      uid: this.idName + '-' + this.idCount,
      status: 'todo',
      title: this.newTaskName,
      description: this.newTaskDescription,
      project: this.activeProject.id,
      sprint: null,
      user: this.newTaskUser.user
    };

    this.showNewTaskLayout = false;
    this.idCount++;
    this.newTaskName = '';
    this.newTaskDescription = '';
    this.newTaskUser = null;

    this.firebaseService.createTask(newTask).subscribe((taskData) => {
      this.taskList.unshift({
        ...newTask,
        id: taskData.id
      });
    }, (error) => {
      console.log('Create Task Error', error);
    });
  }

  editTask(task) {
    this.showEditTaskLayout = true;
    this.taskToEdit = task;

    this.editTaskName = task.title;
    this.editTaskDescription = task.description;

    console.log(task, this.teamList);

    this.teamList.forEach((team) => {
      if (team.user === task.user) {
        this.editTaskUser = team;
      }
    });
  }

  updateTask() {
    const updated = {
      title: this.editTaskName,
      description: this.editTaskDescription,
      user: this.editTaskUser.user,
    };

    this.firebaseService.updateTask(this.taskToEdit, updated).subscribe((data) => {
      console.log('updated', data);

      this.taskToEdit.title = this.editTaskName;
      this.taskToEdit.description = this.editTaskDescription;
      this.taskToEdit.user = this.editTaskUser.user;

      this.showEditTaskLayout = false;
      this.taskToEdit = null;
    });
  }

  deleteTask(task) {
    this.firebaseService.deleteTask(task).subscribe(
      () => {
        this.taskList = this.taskList.filter(t => t !== task);
      },
      () => {
        console.log('DELETE ERROR!');
      }
    );
  }

}
