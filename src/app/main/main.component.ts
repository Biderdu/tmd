import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  private accessAdmin: boolean;
  private user: any;

  private showUsers: boolean;
  private showProjects: boolean;
  private showSelectedProject: boolean;

  private selectedProject: string;

  constructor(private authService: AuthService) {
    this.showUsers = false;
    this.showProjects = true;
    this.showSelectedProject = false;

    this.selectedProject = 'test-project';
  }

  ngOnInit() {
    this.user = this.authService.getUserInfo();
    console.log('GET USER INFO', this.user);

    this.getRights();
  }

  getRights() {
    this.accessAdmin = this.authService.checkRights();
    console.log('getRights', this.accessAdmin);
  }

  setProject(project) {
    this.selectedProject = project.project;

    this.setPanel('selected-project');
  }

  setPanel(panel: string) {
    this.showUsers = panel === 'users';
    this.showProjects = panel === 'projects';
    this.showSelectedProject = panel === 'selected-project';
  }
}
