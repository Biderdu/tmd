import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private login: string;
  private password: string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() { }

  auth() {
    this.authService.auth(this.login, this.password)
    .subscribe(
      (data) => {
        console.log(data);
      },
      (error) => {
        console.log(error);
      },
      () => {
        this.goToMain();
      });
  }

  goToMain() {
    this.router.navigateByUrl('main').then(e => {
      // if (e) {
      //   console.log('Navigation is successful!');
      // } else {
      //   console.log('Navigation has failed!');
      // }
    });
  }

}
