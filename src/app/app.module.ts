import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { HeadPanelComponent } from './main/head-panel/head-panel.component';
import { FirebaseService } from './services/fireb.service';
import { LeftPanelComponent } from './main/left-panel/left-panel.component';
import { UsersPanelComponent } from './main/users-panel/users-panel.component';
import { ProjectsPanelComponent } from './main/projects-panel/projects-panel.component';
import { ProjectPageComponent } from './main/project-page/project-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TaskInfoComponent } from './main/project-page/task-info/task-info.component';
import { SprintsComponent } from './main/project-page/sprints/sprints.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { LinksComponent } from './main/project-page/links/links.component';
import { WorkflowComponent } from './main/project-page/workflow/workflow.component';
import { MatSelectModule } from '@angular/material/select';
import { TeamComponent } from './main/project-page/team/team.component';
import { TasksComponent } from './main/project-page/tasks/tasks.component';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    HeadPanelComponent,
    LeftPanelComponent,
    UsersPanelComponent,
    ProjectsPanelComponent,
    ProjectPageComponent,
    TaskInfoComponent,
    SprintsComponent,
    LinksComponent,
    WorkflowComponent,
    TeamComponent,
    TasksComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    DragDropModule,
    MatSelectModule,
    MatMenuModule,
    RouterModule.forRoot([
      {path: '', component: LoginComponent},
      {path: 'main', component: MainComponent}
    ]),
    BrowserAnimationsModule
  ],
  providers: [FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
