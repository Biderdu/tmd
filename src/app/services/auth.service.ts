import { Injectable } from '@angular/core';
import { FirebaseService } from './fireb.service';
import { Observable } from 'rxjs';

export interface UserInfo {
  name: string;
  role: string;
  user: string;
  status: string;
}

export interface UserItem {
  name: string;
  mail: string;
  status: string;
  role: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: firebase.User;
  private userInfo: any;
  // private userList: Array<UserItem>;
  private userList: Array<any>;

  constructor(private firebaseService: FirebaseService) {
    this.reciveUserList();
    this.setDefaultUser();
  }

  setDefaultUser() {
    this.userInfo = {
      name: 'Admin',
      role: 'admin',
      status: 'active',
      user: 'IrzhZ09L8QOV1Ba5ZKssJPkOxRt1',
    };
  }

  auth(login: string, password: string) {
    return new Observable(subscriber => {

      this.firebaseService.authRequest(login, password).subscribe( (userData) => {
        this.user = userData.user;

        this.firebaseService.getAllInfo(this.user.uid).subscribe((roleData) => {
          if (!roleData.empty) {
            this.userInfo = roleData.docs[0].data();
          }

          subscriber.complete();
        }, (error) => {
          subscriber.error(error);
        });

      }, (error) => {
        subscriber.error(error);
        // console.log('AUTH ERROR:', error);
        // auth/user-not-foun
        // auth/invalid-email
        // auth/wrong-password
      });

    });
  }

  getUserInfo(): UserInfo {
      return {...this.userInfo};
  }

  checkRights(): boolean {
    return this.userInfo.role === 'admin';
  }

  reciveUserList() {
    this.userList = [];

    this.firebaseService.getAllUsers().subscribe((usersData) => {
      usersData.docs.forEach((data) => {
        this.userList.push({
            ...data.data(),
            id: data.id
        });
      });
    });

    // this.userList = [
    //   {
    //     name: 'Admin',
    //     mail: 'admin_tmd@gmail.com',
    //     status: 'active',
    //     role: 'admin',
    //   },
    //   {
    //     name: 'Manager',
    //     mail: 'manager_tmd@gmail.com',
    //     status: 'active',
    //     role: 'manager',
    //   },
    //   {
    //     name: 'User',
    //     mail: 'user_tmd@gmail.com',
    //     status: 'active',
    //     role: 'user',
    //   }
    // ];
  }

  getUsers() {
    return [...this.userList];
  }
}
