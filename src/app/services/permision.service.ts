import { Injectable } from '@angular/core';

const rolesPermisions = {
  admin: {
    project: {
      access: true,
      create: true,
      edit: true,
    },
    sprints: {
      access: true,
      create: true,
      edit: true,
    },
    tasks: {
      access: true,
      create: true,
      edit: true,
    },
    links: {
      access: true,
      create: true,
      edit: true,
    },
    wokflow: {
      access: true,
    }
  },
  manager: {
    project: {
      access: true,
      create: true,
      edit: true,
    },
    sprints: {
      access: true,
      create: true,
      edit: true,
    },
    tasks: {
      access: true,
      create: true,
      edit: true,
    },
    links: {
      access: true,
      create: true,
      edit: true,
    },
    wokflow: {
      access: true,
    }
  },
  user: {
    project: {
      access: true,
      create: true,
      edit: true,
    },
    sprints: {
      access: true,
      create: true,
      edit: true,
    },
    tasks: {
      access: true,
      create: true,
      edit: true,
    },
    links: {
      access: true,
      create: true,
      edit: true,
    },
    wokflow: {
      access: true,
    }
  },
};

@Injectable({
  providedIn: 'root'
})
export class PermisionService {

  constructor() { }

  getPermisions(role) {
    return rolesPermisions[role];
  }
}
