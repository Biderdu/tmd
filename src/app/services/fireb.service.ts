import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { from, Observable } from 'rxjs';
import { NewProject } from '../main/projects-panel/projects-panel.component';
import { NewTask } from '../main/project-page/project-page.component';
import { NewSprint } from '../main/project-page/sprints/sprints.component';
import { NewLink } from '../main/project-page/links/links.component';

const firebaseConfig = {
  apiKey: 'AIzaSyBflFapAAPhTKBfFIMvhu9afUcxK6HZ-UE',
  authDomain: 'task-management-d.firebaseapp.com',
  databaseURL: 'https://task-management-d.firebaseio.com',
  projectId: 'task-management-d',
  storageBucket: 'task-management-d.appspot.com',
  messagingSenderId: '676052229728',
  appId: '1:676052229728:web:3d863380cec0df993bed05',
};

@Injectable({
  providedIn: 'root'
})

export class FirebaseService {
  private app: firebase.app.App;
  private db: firebase.firestore.Firestore;

  constructor() {
    this.init();
  }

  // Initialize Firebase
  init() {
    this.app = firebase.initializeApp(firebaseConfig);
    this.db = firebase.firestore();
  }

  // users
  getAllUsers(): Observable<firebase.firestore.QuerySnapshot> {
    return from(this.db.collection('roles').get());
  }

  getAllInfo(userId: string): Observable<firebase.firestore.QuerySnapshot> {
    return from(this.db.collection('roles').where('user', '==', userId).get());
  }

  authRequest(login: string, password: string): Observable<firebase.auth.UserCredential> {
    return from(firebase.auth().signInWithEmailAndPassword(login, password));
  }

  // projects
  getProjects(userId: string): Observable<firebase.firestore.QuerySnapshot> {
    return from(this.db.collection('projects').where('owner', '==', userId).get());
  }

  createProject(project: NewProject): Observable<firebase.firestore.DocumentReference> {
    return from(this.db.collection('projects').add(project));
  }

  deleteProject(projectId: any) {
    return from(this.db.collection('projects').doc(projectId).delete());
  }

  // tasks
  getTasks(projectId: string): Observable<firebase.firestore.QuerySnapshot> {
    return from(this.db.collection('projects').doc(projectId).collection('tasks').get());
  }

  createTask(task: NewTask): Observable<firebase.firestore.DocumentReference> {
    return from(this.db.collection('projects').doc(task.project).collection('tasks').add(task));
  }

  updateTask(taskInfo, toUpdate) {
    return from(this.db.collection('projects')
                .doc(taskInfo.project)
                .collection('tasks')
                .doc(taskInfo.id).update(toUpdate));
  }

  deleteTask(taskInfo: any) {
    return from(this.db.collection('projects').doc(taskInfo.project).collection('tasks').doc(taskInfo.id).delete());
  }

  // comments
  getComments(taskInfo: any): Observable<firebase.firestore.QuerySnapshot> {
    return from(this.db.collection('projects').doc(taskInfo.project).collection('tasks').doc(taskInfo.id).collection('comments').get());
  }

  createComment(comment: any): Observable<firebase.firestore.DocumentReference> {
    return from(this.db
      .collection('projects')
      .doc(comment.project)
      .collection('tasks')
      .doc(comment.task)
      .collection('comments')
      .add(comment)
    );
  }

  // sprints
  getSprints(projectId: string): Observable<firebase.firestore.QuerySnapshot> {
    return from(this.db.collection('projects').doc(projectId).collection('sprints').get());
  }

  createSprint(sprint: NewSprint): Observable<firebase.firestore.DocumentReference> {
    return from(this.db.collection('projects').doc(sprint.project).collection('sprints').add(sprint));
  }

  updateSprint(sprintInfo, toUpdate) {
    return from(this.db.collection('projects')
                .doc(sprintInfo.project)
                .collection('sprints')
                .doc(sprintInfo.id).update(toUpdate));
  }

  deleteSprint(sprintInfo: any) {
    return from(this.db.collection('projects').doc(sprintInfo.project).collection('sprints').doc(sprintInfo.id).delete());
  }

  // links
  getLinks(projectId: string): Observable<firebase.firestore.QuerySnapshot> {
    return from(this.db.collection('projects').doc(projectId).collection('links').get());
  }

  createLink(link: NewLink): Observable<firebase.firestore.DocumentReference> {
    return from(this.db.collection('projects').doc(link.project).collection('links').add(link));
  }

  deleteLink(link: any) {
    return from(this.db.collection('projects').doc(link.project).collection('links').doc(link.id).delete());
  }

  // team
  getTeam(projectId: string): Observable<firebase.firestore.QuerySnapshot> {
    return from(this.db.collection('projects').doc(projectId).collection('team').get());
  }

  // getAllTeam(): Observable<firebase.firestore.QuerySnapshot> {
  //   return from(this.db.collection('projects').doc;
  // }

  addTeamUser(user: any): Observable<firebase.firestore.DocumentReference> {
    return from(this.db.collection('projects').doc(user.project).collection('team').add(user));
  }

  deleteTeamUser(user: any) {
    return from(this.db.collection('projects').doc(user.project).collection('team').doc(user.id).delete());
  }
}
