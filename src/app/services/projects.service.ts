import { Injectable } from '@angular/core';
import { FirebaseService } from './fireb.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProjectsService {
  private projectsList: Array<any>;
  private sprintsList: Map<string, any>;

  constructor(private firebaseService: FirebaseService) { }

  getAvailableProjects(user: string) {
    return new Observable((subscriber) => {

      this.firebaseService.getProjects(user).subscribe((projectsData) => {
        this.projectsList = [];

        projectsData.docs.forEach((data) => {
          this.projectsList.push({
              ...data.data(),
              id: data.id
          });
        });

        console.log(this.projectsList);

        subscriber.complete();
      },
      (error) => {
        console.log('GET PROJECTS ERROR', error);
        subscriber.error(error);
      });

    });
  }

  getProjectsList() {
    return [...this.projectsList];
  }

  getProjectInfo(projectId: string) {
    return this.projectsList.find((element) => {
      return element.id === projectId;
    });
  }

  deleteProject(projectId: string) {
    return new Observable((subscriber) => {

      this.firebaseService.deleteProject(projectId).subscribe(() => {
        this.projectsList = this.projectsList.filter(prj => prj.id !== projectId);

        console.log(this.projectsList);

        subscriber.complete();
      },
      (error) => {
        console.log('DELETE PROJECT ERROR', error);
        subscriber.error(error);
      });

    });
  }

  getSprints(projectId): Observable<any> {

    return new Observable(subscriber => {

      this.firebaseService.getSprints(projectId).subscribe((sprintsData) => {

        const sprintsList = sprintsData.docs.map((data) => {
          return ({
              ...data.data(),
              id: data.id,
              tasks: []
          });
        });

        subscriber.next(sprintsList);
      }, (error) => {
        console.log('GET SPRINTS ERROR', error);
        subscriber.error([]);
      });

    });

  }

  getTasks(projectId): Observable<any> {

    return new Observable(subscriber => {

      this.firebaseService.getTasks(projectId).subscribe((tasksData) => {

        const taskList = tasksData.docs.map((data) => {
          return({
              ...data.data(),
              id: data.id
          });
        });

        subscriber.next(taskList);
      }, (error) => {
        subscriber.error([]);
        console.log('GET TASKS ERROR', error);
      });

    });

  }

  getTeam(prjectId): Observable<any> {
    return new Observable(subscriber => {

      this.firebaseService.getTeam(prjectId).subscribe((teamData) => {

        const teamList = teamData.docs.map((data) => {
          return {
              ...data.data(),
              id: data.id
          };
        });

        subscriber.next(teamList);

      }, (error) => {
        subscriber.error([]);
      });

    });
  }
}
