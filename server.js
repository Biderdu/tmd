const express = require('express');
const ecstatic = require('ecstatic');
const server = express();
const port = 4200;

server.use(express.static("dist/TMD"));

// server.use(ecstatic({
//     root: `${__dirname}/dist/TMD`,
//     showdir: true,
// }));

server.use("*", function(req, resp) {
    resp.sendFile(`${__dirname}/dist/TMD/index.html`);
});

server.listen(port, () => {
    console.log(`Server listening at ${port}`);
});